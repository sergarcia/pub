# Converts a sent presentation to pdf, uses a 4:3 aspect ratio by default.  For 16:9 use something like 2000x1125
# Arguments:
# 1. name of the sent presentation file
# 3. window width pixels (default 2000)
# 2. window height pixels (default 1500)
# Compatibility notes:
# - Your must be using the X window sytem, which most linux distros do.
# - You need a $TERMINAL environment variable.
# - You will need to adjust key input that makes a programm full screen. In my system that is super+f.
# Requirements:
# - sent and dependencies (tools.suckless.org/sent)
# - imagemagick: comment out the line:
# 	<policy domain="coder" rights="none" pattern="{PS,PS2,PS3,EPS,PDF,XPS}" />
# 	in /etc/ImageMagick-7/policy.xml
# - ghostscript

width=${2:-2000}
height=${3:-1500}

# 0. Simulate screen
OUTID=`xrandr | grep ' connected' | awk '{print $1}'`
xrandr --output $OUTID --panning "${width}x${height}"

# 1. Start presentation
$TERMINAL -e sent $1 &
sleep 2

# 2. Determine number of slides:
N=`sed "s/^#//" $1 | grep -c "^\s*$"`

# 3. Loop through slides and take screenshots
WID=`xdotool search --name "sent" | sed -n 2p` # Second line corresponds to the sent window, first line is the terminal started on step 1
xdotool windowactivate $WID
xdotool key super+f # RESIZE WINDOW TO FULL SCREEN (system specific)

allfiles=''
for (( i=0; i<$N; ++i)); do
	sleep 0.1
	fname="_$i.png"
	allfiles="$allfiles $fname"
	scrot -u $fname
	xdotool key n
done

# 4. Convert screenshots to pdf with imagemagick
eval convert "$allfiles" "$1.pdf"
rm _*.png

# 5. Close out of sent
xdotool key q

# 6. Restore screen
xrandr --output $OUTID --panning 100x100 &> /dev/null
