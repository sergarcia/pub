#!/bin/bash
# Returns an alphabetically sorted list of unique abbreviations. The abbreviations match a pattern of two or more uppercase letters which may be followed by lowercase letters and numbers.

# How to create a table of abbreviations and names? If you have a master abbreviation file named abbr2name.csv and your current abbreviations in abbrv.txt you can simply grep -wf abbrv.tx abbr2name.csv

abbr_pattern='[A-Z][A-Z]\+[a-z]*[0-9]*[a-z]*'
cat $1 | grep -o $abbr_pattern | sort | uniq

